
## Meteorology webapp

# Requirements:
    
    Python 3.6
    pip

# Setup:
It's suggested that the dependencies be installed under 
a virtual environement using the virtualenv python package.

To do so, install virtualenv if needed and initialize virtualenv in the root directory.
    
    pip install virtualenv
    python -m virtualenv env

Activate the virtualenv using
    
    source ./env/Scripts/activate

or on windows cmd

	./env/Scripts/activate

Check that everything works by checking where python is now located (should be under the virtualenv)

	where python

Finally install dependencies using pip
    
    pip install -r requirements.txt

# Running:
To start the webapp set the environment variables and run flask.

    FLASK_APP=main.py FLASK_DEBUG=1 python -m flask run

On windows cmd

    SET FLASK_APP="main.py" & SET FLASK_DEBUG="1" & python -m flask run
