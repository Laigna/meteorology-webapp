
from webapp import app

@app.template_filter('angular')
def angular_filter(value):
    """
    A filter to tell Jinja2 that a variable is for the AngularJS template
    engine.
    If the variable is undefined, its name will be used in the AngularJS
    template, otherwise, its content will be used.
    """

    if value is None:
        return '{{{{{}}}}}'.format(value._undefined_name)
    if type(value) is bool:
        value = repr(value).lower()
    return '{{{{{}}}}}'.format(value)
