
"""
Minification config.
"""

from jac.contrib.flask import JAC
import jinja2
from jac import CompressorExtension

from webapp import app

app.config['COMPRESSOR_DEBUG'] = app.config.get('DEBUG')
app.config['COMPRESSOR_OUTPUT_DIR'] = './webapp/static/dist'
app.config['COMPRESSOR_STATIC_PREFIX'] = '/static/dist'
jac = JAC(app)

env = jinja2.Environment(extensions = [CompressorExtension])
env.compressor_output_dir = './webapp/static/dist'
env.compressor_static_prefix = '/static/dist'
env.compressor_source_dirs = './static_files'
