
from urllib.request import Request, urlopen
from urllib.error import HTTPError, URLError
from datetime import datetime
from itertools import groupby
import pytz
import json

from webapp.services.temperaturesDataDto import TemperaturesDataDto

METEO_SERVICE_URL = 'https://n0e2nz3ku3.execute-api.eu-central-1.amazonaws.com/meteorology/temperature?from={}&to={}'
METEO_X = 'QsZv6H3yA017gXjfTlG1V8oekI6DbZOyelpbBZLi'
#CONSUMPTION_URL = 'https://httpstat.us/404?start={}&end={}' # For testing error codes.

class MeteorologyService(object):
    """
    Service component that fetches data from an 
    auxiliary backend service.
    """

    def getTemperatures(self, timeFromArg, timeToArg, stepArg):
        """
        Fetches the temperatures data synchronously.
        timeFrom and timeTo are required parameters, step is optional and defaults to 'day'.
        """
        # Check if the arguments are valid.
        try: 
            timeFrom = datetime.strptime(timeFromArg, '%Y-%m-%d').replace(tzinfo=pytz.utc)
            timeTo = datetime.strptime(timeToArg, '%Y-%m-%d').replace(tzinfo=pytz.utc)
        except Exception as ex:
            raise ValueError("Argument error: {}.".format(str(ex)))
        
        if timeTo < timeFrom:
            raise ValueError("Argument 'to' {} must be greater than 'from' {}.".format(timeToArg, timeFromArg))

        if stepArg not in [None, 'hour', 'day', 'week', 'month']:
            raise ValueError("Argument 'step' value '{}' out of range [None, 'hour', 'day', 'week', 'month'].".format(stepArg))
        if stepArg is None:
            stepArg = 'hour'
        
        queryUrl = METEO_SERVICE_URL.format(timeFromArg, timeToArg)
        request = Request(queryUrl)
        request.add_header('x-api-key', METEO_X)
        try:
            with urlopen(request) as bodyBytes:
                body = bodyBytes.read().decode('utf-8')
            temperaturesData = self.__getTemperaturesDataFromResponse(body, timeFrom, timeTo, stepArg)
            return TemperaturesDataDto(stepArg, timeFromArg, timeToArg, temperaturesData)
        except URLError as urlErr:
            raise Exception("Error {} occurred during the request to {}.".format(urlErr.reason, queryUrl))
        except HTTPError as httpErr:
            raise Exception("Error {} {} occurred during the request to {}.".format(httpErr.code, httpErr.reason, queryUrl))

    def __getTemperaturesDataFromResponse(self, dataString, timeFrom, timeTo, step):
        """
        Extract and group the data from the response string.
        Returned data has UTC timestamps.
        """
        try:
            data = json.loads(dataString)
            hourlyTemperatures = [[datetime.strptime(x[0], '%Y-%m-%d %H:%M').replace(tzinfo=pytz.utc),
                                   x[1]] 
                                   for x in data['data']]
            hourlyTemperatures = sorted(hourlyTemperatures, key = lambda x: x[0])
            temperaturesData = []
            for k, g in groupby(hourlyTemperatures, lambda x: self.__getDataKeyFromTime(x[0], step)):
                items = list(g)
                temperaturesData.append([k, sum(x[1] for x in items) / len(items)])
        except Exception as ex:
            raise Exception("Error parsing the document: {}.".format(str(ex)))        
        return temperaturesData

    def __getDataKeyFromTime(self, time, step):
        if step.lower() == 'hour':
            return time.strftime('%Y-%m-%d %H:%M')
        elif step.lower() == 'day':
            return time.strftime('%Y-%m-%d')
        elif step.lower() == 'week':
            return time.strftime('%Y-%W')
        elif step.lower() == 'month':
            return time.strftime('%Y-%m')
