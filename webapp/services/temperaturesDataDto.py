
class TemperaturesDataDto(object):

    def __init__(self, step, timeFrom, timeTo, data):
        self.step = step
        self.timeFrom = timeFrom
        self.timeTo = timeTo
        self.data = data

    def toDict(self):
        return {'step': self.step, 
                'from': self.timeFrom, 
                'to': self.timeTo, 
                'data': self.data}
