
(function () {

    var app = angular.module('app', ['moment-picker']);
    app.controller('ctrl', function ($scope, $http) {
        var ctrl = this;
        var today = moment().utc();
        var tenYearsAgo = moment().utc().subtract(10, 'years');
        var oneYearAgo = moment().utc().subtract(1, 'years');

        ctrl.timeStepOptions = ['hour', 'day', 'week', 'month'];
        ctrl.minDate = tenYearsAgo.format('YYYY-MM-DD');
        ctrl.maxDate = today.format('YYYY-MM-DD');
        ctrl.timeFrom = oneYearAgo;
        ctrl.timeTo = ctrl.maxDate;
        ctrl.timeStep = ctrl.timeStepOptions[3];
        ctrl.data = [];
        ctrl.loadingData = false;
        ctrl.errorMessage = "";
        ctrl.orderByProperty = '[0]';
        ctrl.reverse = false;

        ctrl.loadData = function () {
            ctrl.loadingData = true;
            ctrl.errorMessage = "";
            $http.get('/api', {
                params: {
                    'from': ctrl.timeFrom,
                    'to': ctrl.timeTo,
                    'step': ctrl.timeStep
                }
            })
                .then(function (success) {
                    ctrl.loadingData = false;
                    ctrl.data = success.data.data;
                    console.log(success);
                },
                function (error) {
                    ctrl.loadingData = false;
                    ctrl.errorMessage = error.data.error + ' ' + error.data.reason;
                    ctrl.data = [];
                    console.log(error);
                })
        }

        ctrl.orderBy = function (property) {
            if (ctrl.orderByProperty == property) {
                ctrl.reverse = !ctrl.reverse;
            } else {
                ctrl.reverse = false;
                ctrl.orderByProperty = property;
            }
        }

        this.$onInit = function () {
            $scope.ctrl = ctrl;
        }

        this.$onDestroy = function () {

        }
    });

}())
