"""
The main controller.
"""

from datetime import datetime
from flask import render_template

from webapp import app

@app.route('/')
@app.route('/home')
def index():
    return render_template(
        'index.html',
        title='Home Page',
        year=datetime.now().year,
    )

