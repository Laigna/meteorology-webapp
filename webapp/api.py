"""
Api endpoints for our application.
"""

from flask import jsonify, request

from webapp import app, cache, meteorologyService

@app.route('/api', methods = ['GET'])
@cache.cached(timeout = 60, query_string = True)
def getCalories():
    """
    Gets the consumption data for the given parameters as json.
    """
    try:
        temperaturesDataDto = meteorologyService.getTemperatures(
            request.args.get('from'), 
            request.args.get('to'),
            request.args.get('step')
        )
    except ValueError as ex:
        return jsonify({
            'error': "Argument error.", 
            'reason': str(ex)}
        ), 400
    except Exception as ex:
        return jsonify({
            'error': "Unable to fetch data.", 
            'reason': str(ex)}
        ), 503
    return jsonify(temperaturesDataDto.toDict())
