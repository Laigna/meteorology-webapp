"""
The flask application package.
"""

from flask import Flask
from flask_caching import Cache

from webapp.services.meteorologyService import MeteorologyService

app = Flask(__name__)
cache = Cache(app, config={'CACHE_TYPE': 'simple'})
meteorologyService = MeteorologyService()

import webapp.jacConfig
import webapp.angularFilter
import webapp.api
import webapp.views
