import json
from datetime import datetime, timedelta
import math
import random

def lambda_handler(event, context):
    
    if not 'queryStringParameters' in event:
         return {'statusCode': 400, 
                 'body': json.dumps({'message': "Arguments 'from' and 'to' are required."})}
    
    try:
        timeFromArg = event["queryStringParameters"]['from']
        timeToArg = event["queryStringParameters"]['to']
        timeFrom = datetime.strptime(timeFromArg, '%Y-%m-%d')
        timeTo = datetime.strptime(timeToArg, '%Y-%m-%d')
    except Exception as ex:
        return {'statusCode': 400, 
                'body': json.dumps({'message': "Error parsing arguments: " + str(ex)})}
    
    if (timeTo - timeFrom).days > int(10 * 365.25):
        return {'statusCode': 400, 
                'body': json.dumps({'message': "Requested timerange exceeds the maximum of 10 years."})}
    
    try:
        data = generate_data(timeFrom, timeTo)
    except Exception as ex:
        return {'statusCode': 502, 
                'body': json.dumps({'message': "Error: " + str(ex)})}

    body = {'data': data,
            'from': timeFromArg,
            'to': timeToArg}
    return {'statusCode': 200,
            'headers': { },
            'body': json.dumps(body),
            'isBase64Encoded': False}
            
def generate_data(timeFrom, timeTo):
    data = []
    time = timeFrom
    day = timedelta(hours = 1)
    while time < timeTo:
        data.append([time.strftime('%Y-%m-%d %H:%M'), 
                     20 * math.sin(2 * math.pi * (time.month - 1) / 12 - math.pi / 2) + 
                     5 * math.sin(2 * math.pi * time.hour / 24 - math.pi / 2) + 
                     random.uniform(-3.0, 3.0)])
        time = time + day
    return data
